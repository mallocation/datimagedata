﻿using System.Web;
using System.Web.Optimization;

public class BundleConfig
{
    public static void RegisterBundles(BundleCollection bundles)
    {
        //bundles.Add(new ScriptBundle("~/Scripts/jquery").Include(
        //    "~/Scripts/Lib/jquery/jquery-{version}.js",
        //    "~/Scripts/Lib/jquery/jquery.*",
        //    "~/Scripts/Lib/jquery/jquery-ui-{version}.js")
        //);

        bundles.Add(new StyleBundle("~/Content/css").Include(
            "~/Content/datimagedata.css")
        );

        bundles.Add(new StyleBundle("~/Content/css/bootstrap").Include(
            "~/Content/bootstrap.css",
            "~/Content/bootstrap-responsive.css")
        );

        bundles.Add(new ScriptBundle("~/Scripts/javascript").Include(
            "~/Scripts/kinetic.js",
            "~/Scripts/knockout-2.2.1.js")
        );

        bundles.Add(new ScriptBundle("~/Scripts/javascript/datimagedata").Include(
            "~/Scripts/DatImageData/*.js")
        );
    }
}