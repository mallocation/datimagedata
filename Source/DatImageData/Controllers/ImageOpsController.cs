﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;
using System.Drawing;

namespace DatImageData.Controllers
{
    public class ImageOpsController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage BinaryData(string path)
        {
            var webClient = new WebClient();
            var stream = webClient.OpenRead(path);

            var result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new StreamContent(stream);

            if (webClient.ResponseHeaders[HttpResponseHeader.ContentType] != null)
            {                
                var type = webClient.ResponseHeaders[HttpResponseHeader.ContentType];
                result.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(type);
            }

            return result;
        }
    }
}
