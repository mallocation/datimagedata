﻿/// <reference path="../jquery-1.9.1.min.js" />
/// <reference path="../knockout-2.2.1.js" />

function IndexViewModel() {
    var self = this;

    self.imageUrl = ko.observable();
    self.imageDataResult = ko.observable();

    self.getImageDataByUrl = function () {
        self.imageDataResult('');
        getImageData(self.imageUrl());
    };

    function getImageData(url) {
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');
        var img = new Image();

        img.onload = function () {
            ctx.drawImage(this, 0, 0);
            self.imageDataResult(canvas.toDataURL());
        };
        img.src = "/api/imageops/binarydata?path=" + url;
    }
}

$(function () {
    ko.applyBindings(new IndexViewModel(), document.getElementById('view-index'));
});